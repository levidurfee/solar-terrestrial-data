package pkg

type Solar struct {
	SolarData SolarData `xml:"solardata" json:"solardata"`
}

type SolarData struct {
	Source        Source `xml:"source" json:"source"`
	Updated       string `xml:"updated" json:"updated"`
	SolarFlux     string `xml:"solarflux" json:"solarflux"`
	AIndex        string `xml:"aindex" json:"aindex"`
	KIndex        string `xml:"kindex" json:"kindex"`
	KIndexNT      string `xml:"kindexnt" json:"kindexnt"`
	XRay          string `xml:"xray" json:"xray"`
	SunSpots      string `xml:"sunspots" json:"sunspots"`
	HeliumLine    string `xml:"heliumline" json:"heliumline"`
	ProtonFlux    string `xml:"protonflux" json:"protonflux"`
	ElectonFlux   string `xml:"electonflux" json:"electonflux"`
	Aurora        string `xml:"aurora" json:"aurora"`
	Normalization string `xml:"normalization" json:"normalization"`
	LatDegree     string `xml:"latdegree" json:"latdegree"`
	SolarWind     string `xml:"solarwind" json:"solarwind"`
	MagneticField string `xml:"magneticfield" json:"magneticfield"`

	CalculatedConditions    CalculatedConditions    `xml:"calculatedconditions" json:"calculatedconditions"`
	CalculatedVHFConditions CalculatedVHFConditions `xml:"calculatedvhfconditions" json:"calculatedvhfconditions"`

	GeoMagField string `xml:"geomagfield" json:"geomagfield"`
	SignalNoise string `xml:"signalnoise" json:"signalnoise"`
	FOF2        string `xml:"fof2" json:"fof2"`
	MufFactor   string `xml:"muffactor" json:"muffactor"`
	Muf         string `xml:"muf" json:"muf"`
}

type Source struct {
	URL   string `xml:"url,attr" json:"url"`
	Value string `xml:",chardata" json:"value"`
}

type CalculatedConditions struct {
	Bands []Band `xml:"band" json:"bands"`
}

type Band struct {
	Name  string `xml:"name,attr" json:"name"`
	Time  string `xml:"time,attr" json:"time"`
	Value string `xml:",chardata" json:"value"`
}

type CalculatedVHFConditions struct {
	Phenomenons []Phenomenon `xml:"phenomenon" json:"phenomenon"`
}

type Phenomenon struct {
	Name     string `xml:"name,attr" json:"name"`
	Location string `xml:"location,attr" json:"location"`
	Value    string `xml:",chardata" json:"value"`
}
