import {render} from 'preact';
import {App} from './app';
import './style.css';
import './assets/css/reset.css';

render(<App/>, document.getElementById('app')!);
