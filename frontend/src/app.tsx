import './App.css'
import {GetData} from "../wailsjs/go/main/App";
import { pkg } from "../wailsjs/go/models";
import {Component, h} from 'preact';
import { Solar } from './Components/Solar';

const update_interval: number = 15000;

const defaultData: pkg.Solar = new pkg.Solar({
    solardata: {
        source: {
            url: "",
            value: "",
        },
        updated: "",
        solarflux: "",
        aindex: "",
        kindex: "",
        kindexnt: "",
        xray: "",
        sunspots: "",
        heliumline: "",
        protonflux: "",
        electonflux: "",
        aurora: "",
        normalization: "",
        latdegree: "",
        solarwind: "",
        magneticfield: "",
        geomagfield: "",
        signalnoise: "",
        fof2: "",
        muffactor: "",
        muf: "",

        calculatedconditions: {
            bands: [{}]
        },
        calculatedvhfconditions: {
            phenomenon: [{}]
        }
    }
});

type AppState = {
    data: pkg.Solar;
}

export class App extends Component<any, AppState> {
    timer: any;

    constructor(props: any) {
        super(props);

        this.state = {
            data: defaultData
        }
    }

    componentDidMount() {
        GetData().then((data: pkg.Solar) => {
            this.setState({data: data});
        });

        const _this = this;
        this.timer = setInterval(() => {
            GetData().then((data: pkg.Solar) => {
                _this.setState({data: data});
            });
        }, update_interval);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {
        return (
            <>
                <div id="App">
                    <div id="data">
                        <Solar data={this.state.data} />
                    </div>
                    <div id="credit">
                        <p>Data provided by Paul L Herrman (https://www.hamqsl.com/).</p>
                    </div>
                </div>
            </>
        )
    }
}
