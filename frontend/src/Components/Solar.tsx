import { pkg } from "../../wailsjs/go/models";
import {Component, FunctionComponent, h} from 'preact';

type SolarProps = {
    data: pkg.Solar;
};

export class Solar extends Component<SolarProps, any> {
    constructor(props: SolarProps) {
        super(props);
    }

    render() {
        return (
            <>
                <div className="group shadow">
                    <h2>HF Conditions</h2>
                    <HFConditions data={this.props.data.solardata.calculatedconditions} />
                </div>

                <div className="group shadow">
                    <h2>VHF Conditions</h2>
                    <VHFConditions data={this.props.data.solardata.calculatedvhfconditions} />
                </div>

                <div className="group shadow">
                    <h2>Solar-Terrestrial Data</h2>
                    <div id="kvs">
                        <KeyValue keyname="SFI" title="Solar Flux" value={this.props.data.solardata.solarflux} />
                        <KeyValue keyname="A" title="A Index" value={this.props.data.solardata.aindex} />
                        <KeyValue keyname="K" title="K Index" value={this.props.data.solardata.aindex} />
                        <KeyValue keyname="SN" title="Sun Spots" value={this.props.data.solardata.sunspots} />
                        <KeyValue keyname="X-Ray" title="X-Ray" value={this.props.data.solardata.xray} />
                        <KeyValue keyname="304A" title="Helium" value={this.props.data.solardata.heliumline} />
                        <KeyValue keyname="Pf" title="Proton Flux" value={this.props.data.solardata.protonflux} />
                        <KeyValue keyname="Ef" title="Electron Flux" value={this.props.data.solardata.electonflux} />
                        <KeyValue keyname="Aurora" title="Aurora" value={this.props.data.solardata.aurora} />
                        <KeyValue keyname="SW" title="Solar Wind" value={this.props.data.solardata.solarwind} />
                    </div>
                </div>

                <div>
                    Last updated {this.props.data.solardata.updated}
                </div>
            </>
        )
    }
}

function KeyValue(props: any) {
    return (
        <div className="kv">
            <div className="key" title={props.title}>{props.keyname}</div>
            <div className="value">{props.value}</div>
        </div>
    )
}

function HFConditions(props: any) {
    return (
        <div id="hf_conditions" className="conditions">
            <table>
                <thead>
                    <tr>
                        <th className="rounded-left">Band</th>
                        <th>Time</th>
                        <th className="rounded-right">Condition</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data.bands.map((element: any, x: any) => {
                        return (
                            <tr key={x} className={element.time}>
                                <td>{element.name}</td>
                                <td>{element.time}</td>
                                <td>{element.value}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

function VHFConditions(props: any) {
    return (
        <div id="vhf_conditions" className="conditions">
            <table>
                <thead>
                    <tr>
                        <th className="rounded-left">Name</th>
                        <th>Location</th>
                        <th className="rounded-right">Value</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data.phenomenon.map((element: any, x: any) => {
                        return (
                            <tr key={x}>
                                <td>{element.name}</td>
                                <td>{element.location}</td>
                                <td>{element.value}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
