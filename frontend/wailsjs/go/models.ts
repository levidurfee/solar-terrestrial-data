export namespace pkg {
	
	export class Band {
	    name: string;
	    time: string;
	    value: string;
	
	    static createFrom(source: any = {}) {
	        return new Band(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.name = source["name"];
	        this.time = source["time"];
	        this.value = source["value"];
	    }
	}
	export class CalculatedConditions {
	    bands: Band[];
	
	    static createFrom(source: any = {}) {
	        return new CalculatedConditions(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.bands = this.convertValues(source["bands"], Band);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class Phenomenon {
	    name: string;
	    location: string;
	    value: string;
	
	    static createFrom(source: any = {}) {
	        return new Phenomenon(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.name = source["name"];
	        this.location = source["location"];
	        this.value = source["value"];
	    }
	}
	export class CalculatedVHFConditions {
	    phenomenon: Phenomenon[];
	
	    static createFrom(source: any = {}) {
	        return new CalculatedVHFConditions(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.phenomenon = this.convertValues(source["phenomenon"], Phenomenon);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	
	export class Source {
	    url: string;
	    value: string;
	
	    static createFrom(source: any = {}) {
	        return new Source(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.url = source["url"];
	        this.value = source["value"];
	    }
	}
	export class SolarData {
	    source: Source;
	    updated: string;
	    solarflux: string;
	    aindex: string;
	    kindex: string;
	    kindexnt: string;
	    xray: string;
	    sunspots: string;
	    heliumline: string;
	    protonflux: string;
	    electonflux: string;
	    aurora: string;
	    normalization: string;
	    latdegree: string;
	    solarwind: string;
	    magneticfield: string;
	    calculatedconditions: CalculatedConditions;
	    calculatedvhfconditions: CalculatedVHFConditions;
	    geomagfield: string;
	    signalnoise: string;
	    fof2: string;
	    muffactor: string;
	    muf: string;
	
	    static createFrom(source: any = {}) {
	        return new SolarData(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.source = this.convertValues(source["source"], Source);
	        this.updated = source["updated"];
	        this.solarflux = source["solarflux"];
	        this.aindex = source["aindex"];
	        this.kindex = source["kindex"];
	        this.kindexnt = source["kindexnt"];
	        this.xray = source["xray"];
	        this.sunspots = source["sunspots"];
	        this.heliumline = source["heliumline"];
	        this.protonflux = source["protonflux"];
	        this.electonflux = source["electonflux"];
	        this.aurora = source["aurora"];
	        this.normalization = source["normalization"];
	        this.latdegree = source["latdegree"];
	        this.solarwind = source["solarwind"];
	        this.magneticfield = source["magneticfield"];
	        this.calculatedconditions = this.convertValues(source["calculatedconditions"], CalculatedConditions);
	        this.calculatedvhfconditions = this.convertValues(source["calculatedvhfconditions"], CalculatedVHFConditions);
	        this.geomagfield = source["geomagfield"];
	        this.signalnoise = source["signalnoise"];
	        this.fof2 = source["fof2"];
	        this.muffactor = source["muffactor"];
	        this.muf = source["muf"];
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class Solar {
	    solardata: SolarData;
	
	    static createFrom(source: any = {}) {
	        return new Solar(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.solardata = this.convertValues(source["solardata"], SolarData);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	

}

