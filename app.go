package main

import (
	"changeme/pkg"
	"context"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
)

// App struct
type App struct {
	ctx context.Context
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
}

// Greet returns a greeting for the given name
func (a *App) Greet(name string) string {
	return fmt.Sprintf("Hello %s, It's show time!", name)
}

func (a *App) GetData() pkg.Solar {
	resp, err := http.Get("https://cache.ke8lmf.com/solar.xml")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	data := pkg.Solar{}

	err = xml.Unmarshal(body, &data)
	if err != nil {
		panic(err)
	}

	return data
}
