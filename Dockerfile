FROM golang:1.21
RUN go install github.com/wailsapp/wails/v2/cmd/wails@latest
RUN apt-get update && apt-get install -y npm gcc libgtk-3-dev libwebkit2gtk-4.0-dev wget
RUN wget https://github.com/upx/upx/releases/download/v4.2.1/upx-4.2.1-amd64_linux.tar.xz
RUN tar -xf upx-4.2.1-amd64_linux.tar.xz
RUN mv upx-4.2.1-amd64_linux/upx /usr/local/bin
